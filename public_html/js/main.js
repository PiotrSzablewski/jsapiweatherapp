var appid = '1cbba56d4921bf7931db619e962de97c';


function getDateFromTimestamp(unix_timestamp) {
    var date = new Date(unix_timestamp * 1000);
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();

    // Will display time in 10:30:23 format
    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    return formattedTime;
}


document.getElementById("btn_search").addEventListener('click', function () {
    console.log('click');
    var city = $('#city').val();
    var elementStatus = $("#status");
    var elementData = $("#data");
    var temp = document.getElementById('temp_type').checked;
    var sign = "&#8451;";
    if (temp) {
        temp = 'imperial';
        sign = "&#8457;";
    } else {
        temp = 'metric';
        sign = "&#8451;";
    }
    elementData.empty();
    elementStatus.empty().append("Wysyłam zapytanie...");
    if (city.trim() == '') {
        elementStatus.empty().append("Błąd, nie podano miasta!");
        return;
    }
    $.ajax({
        url: "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=" + appid + "&units=" + temp,
        type: 'GET',
        dataType: 'jsonp',
        success: function (data) { // wywołanie się powiodło
            console.log(data);
            elementStatus.empty().append("Odpowiedż serwera:");
            elementData.empty();
            elementData.append("<div class='city'> <p>Miejsce: " + data.name + ", " + data.sys.country + " - Szerokość geo.:" + data.coord.lat + ", Długość geo.:" + data.coord.lon + " </p></div>");
            elementData.append("<div class='clouds'> <p>Zachmurzenie: " + data.clouds.all + "% </p></div>");
            elementData.append("<div class='time'> <p>Godzina aktualizacji: " + getDateFromTimestamp(data.dt) + " </p></div>");
            elementData.append("<div class='main_data'><p>");
            elementData.append("Wilgotność: " + data.main.humidity + "%</br>");
            elementData.append("Ciśnienie: " + data.main.pressure + "</br>");
            elementData.append("Temperatura: " + data.main.temp + " " + sign + " </br>");
            elementData.append("Temperatura(min): " + data.main.temp_max + " " + sign + " </br>");
            elementData.append("Temperatura(max): " + data.main.temp_min + " " + sign + " </br>");
            elementData.append("</p></div>");
        },
        error: function () {
            elementStatus.empty().append("Błąd zapytania!");
        }
    });
});
