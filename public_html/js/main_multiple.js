var appid = '1cbba56d4921bf7931db619e962de97c';

function createCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function getDateFromTimestamp(unix_timestamp) {
    var date = new Date(unix_timestamp * 1000);
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();

    // Will display time in 10:30:23 format
    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    return formattedTime;
}

document.getElementById("btn_add").addEventListener('click', function () {
    var obecne_ciastka = readCookie("miasta");
    console.log(obecne_ciastka);

    if (obecne_ciastka == "") {
        obecne_ciastka += $('#city').val();
    } else {
        obecne_ciastka += ":" + $('#city').val();
    }
    console.log(obecne_ciastka);

    createCookie("miasta", obecne_ciastka, 7);
});

setInterval(function () {
    $("#cities").empty();
    var obecne_ciastka = readCookie("miasta");

    var miasta = obecne_ciastka.split(":");
    obecne_ciastka = obecne_ciastka.replace("::", ":");
    for (var i = 0; i < miasta.length; i++) {
        if (miasta[i].trim() == "") {
            continue;
        }

        $("#cities").append("<p><a style=\"width:200px\" class=\"btn btn-default\" >" + miasta[i] + "</a><a class=\"btn btn-default\" href=\"#\" role=\"button\" id=\"btn_del_" + miasta[i] + "\">Usuń</a></p>");
        $("#btn_del_" + miasta[i]).on('click', function () {
            console.log("loop");
            var obecne_ciastka = readCookie("miasta");
            var result = obecne_ciastka.replace(this.id.replace("btn_del_", ""), "");
            result = result.replace("::", ":");
            createCookie("miasta", result, 7);
        });
    }
}, 1000);

function refreshWeather() {
    $("#cities_weather").empty();
    var container = $("#cities_weather");
    var obecne_ciastka = readCookie("miasta");

    var temp = document.getElementById('temp_type').checked;
    var sign = "&#8451;";
    if (temp) {
        temp = 'imperial';
        sign = "&#8457;";
    } else {
        temp = 'metric';
        sign = "&#8451;";
    }
    var miasta = obecne_ciastka.split(":");
    for (var i = 0; i < miasta.length; i++) {
        if (miasta[i].trim() == "") {
            continue;
        }
        container.append("<div id=\"miasto_" + miasta[i] + "\"></div>");

        // odrysowanie pogody
        $.ajax({
            url: "http://api.openweathermap.org/data/2.5/weather?q=" + miasta[i] + "&appid=" + appid + "&units="+temp,
            type: 'GET',
            dataType: 'jsonp',
            success: function (data) { // wywołanie się powiodło
                console.log(data);
                var containerOfCity = $("#miasto_" + data.name);
                console.log(containerOfCity);
                containerOfCity.empty();
                containerOfCity.append("<div class='city'> <p>Miejsce: " + data.name + ", " + data.sys.country + " - Szerokość geo.:" + data.coord.lat + ", Długość geo.:" + data.coord.lon + " </p></div>");
                containerOfCity.append("<div class='clouds'> <p>Zachmurzenie: " + data.clouds.all + "% </p></div>");
                containerOfCity.append("<div class='time'> <p>Godzina aktualizacji: " + getDateFromTimestamp(data.dt) + " </p></div>");
                containerOfCity.append("<div class='main_data'><p>");
                containerOfCity.append("Wilgotność: " + data.main.humidity + "%</br>");
                containerOfCity.append("Ciśnienie: " + data.main.pressure + "</br>");
                containerOfCity.append("Temperatura: " + data.main.temp + " " + sign + " </br>");
                containerOfCity.append("Temperatura(min): " + data.main.temp_max + " " + sign + " </br>");
                containerOfCity.append("Temperatura(max): " + data.main.temp_min + " " + sign + " </br>");
                containerOfCity.append("</p></div><hr>");
            },
            error: function () {
                containerOfCity.empty().append("Błąd zapytania!");
            }
        });
    }
}

setInterval(refreshWeather, 10000);
refreshWeather();
